#ifndef S21_PATTERN_H_
#define S21_PATTERN_H_

#include <stddef.h>
#include <stdlib.h>
#include <string.h>

// Структура для хранения шаблонов
typedef struct pattern_t {
  char *line;
  size_t size;
  int count;
} pattern_t;

// --- Функции для работы со структурой шаблонов

// Функция инициализации шаблона (выделение памяти)
int s21_pattern_init(pattern_t *patterns);
// Функция добавления искомого слова в строку шаблона
int s21_pattern_add(pattern_t *patterns, char *str);
// Функция удаления шаблона
void s21_pattern_delete(pattern_t *patterns);

#endif  // S21_PATTERN_H_