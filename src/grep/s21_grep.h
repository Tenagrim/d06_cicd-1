#ifndef S21_GREP_H_
#define S21_GREP_H_

#include <regex.h>

#include "s21_pattern.h"

//  Структура флагов
typedef struct flags_t {
  int e;
  int i;
  int v;
  int c;
  int l;
  int n;
  int h;
  int s;
  int f;
  int o;
} flags_t;

// Структура для передачи свойств файла
typedef struct file_t {
  char name[255];
  int lines;
  int relevant_lines;
} file_t;

// --- Функции чтения и обработки текста
// Функция
int s21_file_init(file_t *exfile, const char *filename);
// Функция для чтения содержимого файла
int s21_file_read(file_t exfile, flags_t flags, regex_t *regex, int files);
// Функция поиска подстроки
int s21_substr_search(const char *str, flags_t flags, regex_t *regex);
// Функция вывода строки на экран
void s21_line_print(flags_t flags, file_t exfile, int files, const char *str);

// --- Функции установки флагов и парсинга аргументов
// Функция парсинга аргументов
int s21_grep_options_parse(flags_t *flags, pattern_t *patterns, int argc,
                           char *argv[]);
// Функция установка флага
int s21_grep_flag_set(flags_t *flags, pattern_t *patterns, int option,
                      char *optionarg);
// Функция парсинга шаблонов из файла
int s21_grep_patterns_from_file(pattern_t *patterns, const char *filename);

// --- Функции работы с регулярными выражениями
// Функция компиляции регулярного выражения
int s21_regex_compile(regex_t *regex, flags_t flags, pattern_t patterns);
// Функция поиска по регулярному выражению в строке
int s21_regex_executive(regex_t *regex, char *str);
// Функция разбора ошибок при компиляции и поиске
void s21_regex_print_error(int error);
// Функция вывода на экран всех совпадений вместо строк
int s21_regex_print_matches(regex_t *regex, flags_t flags, file_t exfile,
                            int files, char *str);

#endif  // S21_GREP_H_