#define _GNU_SOURCE

#include "s21_grep.h"

#include <getopt.h>
#include <stdio.h>

int main(int argc, char *argv[]) {
  flags_t flags = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
  pattern_t patterns;
  s21_pattern_init(&patterns);
  regex_t regex;
  if (s21_grep_options_parse(&flags, &patterns, argc, argv)) {
    if ((patterns.count == 0) && (optind < argc))
      s21_pattern_add(&patterns, argv[optind++]);
    if (s21_regex_compile(&regex, flags, patterns)) {
      int files = argc - optind;
      while (optind < argc) {
        file_t explore_file;
        s21_file_init(&explore_file, argv[optind++]);
        s21_file_read(explore_file, flags, &regex, files);
      }
      regfree(&regex);
    }
  }
  s21_pattern_delete(&patterns);
  return 0;
}

int s21_grep_options_parse(flags_t *flags, pattern_t *patterns, int argc,
                           char *argv[]) {
  int error = 0, option = 0;
  while (((option = getopt(argc, argv, "e:ivclnhf:o")) != -1) && (!error)) {
    error = s21_grep_flag_set(flags, patterns, option, optarg);
  }
  return !error;
}

int s21_grep_flag_set(flags_t *flags, pattern_t *patterns, int option,
                      char *optionarg) {
  int error = 0;
  if (option == 'e') {
    flags->e = 1;
    s21_pattern_add(patterns, optionarg);
  } else if (option == 'i') {
    flags->i = 1;
  } else if (option == 'v') {
    flags->v = 1;
  } else if (option == 'c') {
    flags->c = 1;
  } else if (option == 'l') {
    flags->l = 1;
  } else if (option == 'n') {
    flags->n = 1;
  } else if (option == 'h') {
    flags->h = 1;
  } else if (option == 's') {
    flags->s = 1;
  } else if (option == 'f') {
    flags->f = 1;
    if (!s21_grep_patterns_from_file(patterns, optionarg)) {
      error = 1;
      if (!flags->s)
        fprintf(stderr, "s21_grep: %s: No such file or directory\n", optionarg);
    }
  } else if (option == 'o') {
    flags->o = 1;
  } else {
    error = 1;
    fprintf(stderr, "s21_grep: Invalid key - \"%c\"\n", option);
  }
  return error;
}

int s21_grep_patterns_from_file(pattern_t *patterns, const char *filename) {
  int error = 0;
  char *line = NULL;
  size_t line_length = 0;
  FILE *filep = fopen(filename, "r");
  if (filep != NULL) {
    while (getline(&line, &line_length, filep) != -1) {
      if (line != NULL) {
        if (line[strlen(line) - 1] == '\n') line[strlen(line) - 1] = '\0';
        s21_pattern_add(patterns, line);
      }
    }
    fclose(filep);
  } else {
    error = 1;
  }
  if (line != NULL) free(line);
  return !error;
}

int s21_file_init(file_t *exfile, const char *filename) {
  int error = 0;
  if (strlen(filename) <= 255) {
    strcpy(exfile->name, filename);
    exfile->lines = 0;
    exfile->relevant_lines = 0;
  } else {
    error = 1;
  }
  return !error;
}

int s21_file_read(file_t exfile, flags_t flags, regex_t *regex, int files) {
  char *line = NULL;
  size_t line_length = 0;
  FILE *filep = fopen(exfile.name, "r");
  if (filep != NULL) {
    while (getline(&line, &line_length, filep) != -1) {
      if (line != NULL) {
        exfile.lines++;
        if ((s21_regex_executive(regex, line) && !flags.v) ||
            (!s21_regex_executive(regex, line) && flags.v)) {
          exfile.relevant_lines++;
          if (!flags.c && !flags.l) {
            if (flags.o)
              s21_regex_print_matches(regex, flags, exfile, files, line);
            else
              s21_line_print(flags, exfile, files, line);
          }
        }
      }
    }
    if (flags.l && exfile.relevant_lines) printf("%s\n", exfile.name);
    if (!flags.l && flags.c) {
      if (!flags.h && files > 1) printf("%s:", exfile.name);
      printf("%d\n", exfile.relevant_lines);
    }
    fclose(filep);
  } else {
    if (!flags.s)
      fprintf(stderr, "s21_grep: %s: No such file or directory\n", exfile.name);
  }
  if (line != NULL) free(line);
  return 0;
}

void s21_line_print(flags_t flags, file_t exfile, int files, const char *str) {
  if (!flags.h && files > 1) printf("%s:", exfile.name);
  if (flags.n) printf("%d:", exfile.lines);
  printf("%s", str);
  if ((str[strlen(str) - 1] != '\n')) printf("%c", '\n');
}

int s21_regex_compile(regex_t *regex, flags_t flags, pattern_t patterns) {
  int error_compile = 1;
  error_compile =
      regcomp(regex, patterns.line,
              REG_NEWLINE | ((flags.e || flags.f) ? REG_EXTENDED : 0) |
                  ((flags.i) ? REG_ICASE : 0));
  if (error_compile) s21_regex_print_error(error_compile);
  return !error_compile;
}

int s21_regex_executive(regex_t *regex, char *str) {
  int error_find = 1;
  size_t nmatch = 1;
  regmatch_t pmatch[1];
  error_find = regexec(regex, str, nmatch, pmatch, 0);
  if (error_find) s21_regex_print_error(error_find);
  return !error_find;
}

int s21_regex_print_matches(regex_t *regex, flags_t flags, file_t exfile,
                            int files, char *str) {
  size_t nmatch = 1;
  regmatch_t pmatch[1];
  int error = 0;
  error = regexec(regex, str, nmatch, pmatch, 0);
  if (error) {
    s21_regex_print_error(error);
  } else {
    char *result;
    int result_length = pmatch[0].rm_eo - pmatch[0].rm_so;
    result = (char *)malloc((result_length + 1) * (sizeof(char)));
    strncpy(result, &str[pmatch[0].rm_so], result_length);
    result[result_length] = '\0';
    s21_line_print(flags, exfile, files, result);
    free(result);
    if ((long unsigned int)pmatch[0].rm_so + result_length < strlen(str)) {
      s21_regex_print_matches(regex, flags, exfile, files,
                              &str[pmatch[0].rm_eo]);
    }
  }
  return !error;
}

void s21_regex_print_error(int error) {
  if (error == REG_BADPAT)
    fprintf(stderr, "Regex: Invalid regular expression\n");
  else if (error == REG_ECOLLATE)
    fprintf(stderr, "Regex: Invalid collating element referenced\n");
  else if (error == REG_ECTYPE)
    fprintf(stderr, "Regex: Invalid character class type referenced\n");
  else if (error == REG_EESCAPE)
    fprintf(stderr, "Regex: Trailing \\ in pattern\n");
  else if (error == REG_ESUBREG)
    fprintf(stderr, "Regex: Number in \"\\digit\" invalid or in error\n");
  else if (error == REG_EBRACK)
    fprintf(stderr, "Regex: \"[]\" imbalance\n");
  else if (error == REG_EPAREN)
    fprintf(stderr, "Regex: \"\\(\\)\" or \"()\" imbalance\n");
  else if (error == REG_EBRACE)
    fprintf(stderr, "Regex: \"\\{\\}\" imbalance\n");
  else if (error == REG_BADBR)
    fprintf(stderr,
            "Regex: Content of \"\\{\\}\" invalid: not a number, number too "
            "large, more than two numbers, first larger than second\n");
  else if (error == REG_ERANGE)
    fprintf(stderr, "Regex: Invalid endpoint in range expression\n");
  else if (error == REG_ESPACE)
    fprintf(stderr, "Regex: Out of memory\n");
  else if (error == REG_BADRPT)
    fprintf(
        stderr,
        "Regex: '?' , '*' , or '+' not preceded by valid regular expression\n");
}
