#include "s21_pattern.h"

int s21_pattern_init(pattern_t *patterns) {
  int error = 0;
  patterns->line = calloc(1, sizeof(char));
  if (patterns->line != NULL) {
    strcpy(patterns->line, "\0");
    patterns->count = 0;
    patterns->size = 0;
  } else {
    error = 1;
  }
  return !error;
}

int s21_pattern_add(pattern_t *patterns, char *str) {
  int error = 0;
  if (patterns->count == 0) {
    patterns->line = realloc(patterns->line, (strlen(str) + 1) * sizeof(char));
    if (patterns->line != NULL)
      strcpy(patterns->line, str);
    else
      error = 1;
  } else {
    patterns->line = realloc(patterns->line,
                             (patterns->size + strlen(str) + 1) * sizeof(char));
    if (patterns->line != NULL) {
      strcat(patterns->line, "|");
      strcat(patterns->line, str);
    } else {
      error = 1;
    }
  }
  if (!error) {
    patterns->count++;
    patterns->size = strlen(patterns->line) + 1;
  }
  return !error;
}

void s21_pattern_delete(pattern_t *patterns) {
  if (patterns->line != NULL) free(patterns->line);
  patterns->count = 0;
  patterns->size = 0;
}