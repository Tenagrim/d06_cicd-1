#!/bin/bash

scp cat/s21_cat tenagrim@192.168.18.149:/usr/local/bin 2> errors.txt
scp grep/s21_grep tenagrim@192.168.18.149:/usr/local/bin 2> errors.txt
ERRORS="$(grep -e lost -e fail -e scp errors.txt | wc -l)"
cat errors.txt
rm errors.txt
if [ $ERRORS != 0 ]
then
  echo 'Deploy failed :('
  exit 1
fi
echo 'Deploy succeeded ^_^'
exit 0