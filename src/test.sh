#!/bin/bash
cd cat
ERRORS1=$(bash test_executive.sh | grep Failed | awk '{print $2}')
cd ../grep
ERRORS2=$(bash test_executive.sh | grep Failed | awk '{print $2}')

if [ $ERRORS1 != 0 ] ||
   [ $ERRORS2 != 0 ]
then
  echo 'Integration-test failed :('
  exit 1
fi
echo 'Integration-test succeeded ^_^'
exit 0