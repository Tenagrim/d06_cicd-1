## Part 1. Настройка gitlab-runner
1. Подними виртуальную машину **Ubuntu Server 22.04 LTS**.
2. Скачай и установи на виртуальную машину **gitlab-runner**.
3. Запусти **gitlab-runner** и зарегистрируй его для использования в текущем проекте (DO6_CICD).
    - Для регистрации понадобятся URL и токен, которые можно получить на страничке задания на платформе.

        ![Рисунок 1-1](screens/1-1.png)
        
        _Рисунок 1-1. Регистрация gitlab-runner_

## Part 2. Сборка

- _CI - Continuous Integration (Непрерывная интеграция)_
- _CD - Continuous Delivery (Непрерывная поставка)_

Напиши этап для CI по сборке приложений из проекта C2_SimpleBashUtils.

- В файле gitlab-ci.yml добавь этап запуска сборки через мейк файл из проекта C2.
- Файлы, полученные после сборки (артефакты), сохрани в произвольную директорию со сроком хранения 30 дней.

    ![Рисунок 2-1](screens/2-1.png)

    _Рисунок 2-1. gitlab-ci.yml_

## Part 3. Тест кодстайла

Напиши этап для CI, который запускает скрипт кодстайла (clang-format):
- Если кодстайл не прошел, то «зафейли» пайплайн.
- В пайплайне отобрази вывод утилиты clang-format.

    ![Рисунок 3-1](screens/3-1.png)

    _Рисунок 3-1. Скрипт для проверка клангом_

    ![Рисунок 3-2](screens/3-2.png)

    _Рисунок 3-2. Пайплайны с разными результатами_

    ![Рисунок 3-3](screens/3-3.png)

    _Рисунок 3-3. Вывод зафейлинного пайплайна_

    ![Рисунок 3-4](screens/3-4.png)

    _Рисунок 3-4. Вывод успешного пайплайна_

## Part 4. Интеграционные тесты

Напиши этап для CI, который запускает твои интеграционные тесты из того же проекта:

- Запусти этот этап автоматически только при условии, если сборка и тест кодстайла прошли успешно.

- Если тесты не прошли, то «зафейли» пайплайн.

- В пайплайне отобрази вывод, что интеграционные тесты успешно прошли / провалились.

    ![Рисунок 4-1](screens/4-1.png)

    _Рисунок 4-1. Скрипт интеграционных тестов_

    ![Рисунок 4-2](screens/4-2.png)

    _Рисунок 4-2. Этап для запуска скрипта_

    ![Рисунок 4-3](screens/4-3.png)

    _Рисунок 4-3. Вывод зафейлиного пайплайна_

    ![Рисунок 4-4](screens/4-4.png)

    _Рисунок 4-4. Вывод успешного пайплайна_

    ![Рисунок 4-5](screens/4-5.png)

    _Рисунок 4-5. Прерывание пайплайна при неуспешной проверке на стиль_

## Part 5. Этап деплоя

1. Подними вторую виртуальную машину Ubuntu Server 22.04 LTS.
    - После запуска второй машины надо соединить ее с первой. Для этого необходимо включить второй ethernet-интерфейс на машинах и задать им статические IP-адреса из одной подсети путем редактирования `/etc/netplan/00-installer-config.yaml`. В моем случае IP-адреса:
        - 192.168.1.1/16 - машина с gitlab-runner;
        - 192.168.1.2/16 - машина, куда загружаются исполняемые файлы.
    - Для того чтобы gitlab-runner мог копировать исполняемые файлы в системную папку `/usr/local/bin` на deploy-машину, необходимо:
        - сгенерировать ssh-ключ под пользователем gitlab-runner;
        - скопировать этот ssh-ключ в файл `/root/.ssh/authorized_keys` на deploy-машине;
        - разрешить подключение по ssh к руту без ввода пароля, изменив конфиг `/etc/ssh/sshd_config` и введя свойство `PermitRootLogin yes`;
    - Если все сделано правильно, gitlab-runner сможет подключаться к deploy-машине под рутом, защитив соединение ssh-ключом.
2. Напиши этап для CD, который «разворачивает» проект на другой виртуальной машине.
3. Запусти этот этап вручную при условии, что все предыдущие этапы прошли успешно.
4. Напиши bash-скрипт, который при помощи ssh и scp копирует файлы, полученные после сборки (артефакты), в директорию /usr/local/bin второй виртуальной машины.

    ![Рисунок 5-1](screens/5-1.png)

    _Рисунок 5-1. Скрипт копирования артефактов_

5. В файле gitlab-ci.yml добавь этап запуска написанного скрипта.

    ![Рисунок 5-2](screens/5-2.png)

    _Рисунок 5-2. Запуск скрипта в пайлайне вручную_

6. В случае ошибки «зафейли» пайплайн.

    ![Рисунок 5-3](screens/5-3.png)

    _Рисунок 5-3. Зафейлиный пайплайн_

7. В результате ты должен получить готовые к работе приложения из проекта C2_SimpleBashUtils (s21_cat и s21_grep) на второй виртуальной машине.

    ![Рисунок 5-4](screens/5-4.png)

    _Рисунок 5-4. Готовые к работе приложения на Deploy-машине_

## Part 6. Дополнительно. Уведомления

Настрой уведомления о успешном/неуспешном выполнении пайплайна через бота с именем «[твой nickname] DO6 CI/CD» в Telegram.

- Текст уведомления должен содержать информацию об успешности прохождения как этапа CI, так и этапа CD.
- В остальном текст уведомления может быть произвольным.

    ![Рисунок 6-1](screens/6-1.png)

    _Рисунок 6-1. Создание бота в телеграм_

    ![Рисунок 6-2](screens/6-2.png)

    _Рисунок 6-2. Скрипт отправки сообщения с отчетом об этапе_

    ![Рисунок 6-3](screens/6-3.png)

    _Рисунок 6-3. Примеры запуска скрипта в пайплайне после выполнения основного скрипта каждого этапа_

    ![Рисунок 6-4](screens/6-4.png)

    _Рисунок 6-4. Примеры сообщений о выполненных/зафейленых этапах_