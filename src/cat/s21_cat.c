#include "s21_cat.h"

#include <getopt.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

int main(int argc, char *argv[]) {
  cat_flags flags = {0, 0, 0, 0, 0, 0};
  s21_arg_parse(argc, argv, &flags);
  return 0;
}

void s21_arg_parse(int argc, char *argv[], cat_flags *flags) {
  int i = 1;
  int noerror = 1;
  while (i < argc && noerror) {
    if (argv[i][0] == '-')
      if (argv[i][1] == '-')
        noerror = s21_set_long_flag(flags, argv[i]);
      else
        noerror = s21_set_short_flags(flags, argv[i]);
    else
      break;
    i++;
  }
  while (i < argc && noerror) {
    s21_print_file(argv[i], *flags);
    i++;
  }
}

int s21_set_short_flag(cat_flags *flags, char option) {
  int noerror = 1;
  if (option == 'b') {
    flags->b = 1;
  } else if (option == 'e') {
    flags->e = 1;
    flags->v = 1;
  } else if (option == 'E') {
    flags->e = 1;
  } else if (option == 'n') {
    flags->n = 1;
  } else if (option == 's') {
    flags->s = 1;
  } else if (option == 't') {
    flags->t = 1;
    flags->v = 1;
  } else if (option == 'T') {
    flags->t = 1;
  } else if (option == 'v') {
    flags->v = 1;
  } else {
    fprintf(stderr,
            "s21_cat: illegal option -- %c\nusage: cat [-benstuv] [file ...]\n",
            option);
    noerror = 0;
  }
  if (flags->b && flags->n) flags->n = 0;

  return noerror;
}

int s21_set_short_flags(cat_flags *flags, char *options) {
  int noerror = 1;
  for (size_t i = 1; i < strlen(options) && noerror; i++)
    noerror = s21_set_short_flag(flags, options[i]);
  return noerror;
}

int s21_set_long_flag(cat_flags *flags, char *option) {
  int noerror = 1;
  if (strcmp(option, "--number-nonblank"))
    s21_set_short_flag(flags, 'b');
  else if (strcmp(option, "--number"))
    s21_set_short_flag(flags, 'n');
  else if (strcmp(option, "--squeeze-blank"))
    s21_set_short_flag(flags, 's');
  else
    noerror = 0;
  return noerror;
}

void s21_print_file(char *const argument, cat_flags flags) {
  FILE *filep = fopen(argument, "r");
  if (filep != NULL) {
    int str_counter = 0;
    int str_empty = 0;
    char previous_char = '\0';
    while (!feof(filep) && !ferror(filep)) {
      int ch = getc(filep);
      if (ch != EOF) {
        if (ch == '\n' && previous_char == '\n' && flags.s) {
          str_empty++;
          if (str_empty > 1) continue;
        } else {
          str_empty = 0;
        }
        if (previous_char == '\n' || previous_char == '\0') {
          s21_mod_b(ch, &str_counter, flags.b);
          s21_mod_n(ch, &str_counter, flags.n);
        }
        s21_mod_e(ch, flags.e);
        s21_mod_t(&ch, flags.t);
        s21_mod_v(&ch, flags.v);
        putchar(ch);
        previous_char = ch;
      }
    }
    fclose(filep);
  } else {
    fprintf(stderr, "cat: %s: No such file or directory\n", argument);
  }
}

void s21_mod_b(int ch, int *counter, int flag) {
  if (flag && (ch != '\0') && (ch != '\n')) {
    *counter += 1;
    printf("%6d\t", *counter);
  }
}

void s21_mod_n(int ch, int *counter, int flag) {
  if (flag && (ch != '\0')) {
    *counter += 1;
    printf("%6d\t", *counter);
  }
}

void s21_mod_e(int ch, int flag) {
  if (flag && ch == '\n') putchar('$');
}

void s21_mod_t(int *ch, int flag) {
  if (flag && *ch == '\t') {
    putchar('^');
    *ch = 'I';
  }
}

void s21_mod_v(int *ch, int flag) {
  if (flag &&
      ((*ch >= 0 && *ch <= 8) || (*ch >= 11 && *ch <= 31) || *ch == 127)) {
    putchar('^');
    if (*ch >= 127)
      *ch -= 64;
    else
      *ch += 64;
  }
}
