#ifndef S21_CAT_H_
#define S21_CAT_H_

typedef struct {
  int b;
  int e;
  int n;
  int s;
  int t;
  int v;
} cat_flags;

// --- Основные функции программы
// Функция для парсинга аргументов
void s21_arg_parse(int argc, char *argv[], cat_flags *flags);
// Функция вывода данных из файла с учетом флагом
void s21_print_file(char *const argument, cat_flags flags);
// Очистка содержимого строки
void str_clear(char *str);

// --- Функция установки флагов
// Функция установки короткого флага
int s21_set_short_flag(cat_flags *flags, char option);
// Функция установки коротких флагов
int s21_set_short_flags(cat_flags *flags, char *options);
// Функция установки длинного флага
int s21_set_long_flag(cat_flags *flags, char *option);

// --- Функции обработки флагов
void s21_mod_b(int ch, int *counter, int flag);
void s21_mod_n(int ch, int *counter, int flag);
void s21_mod_e(int ch, int flag);
void s21_mod_t(int *ch, int flag);
void s21_mod_v(int *ch, int flag);

#endif  // S21_CAT_H_
